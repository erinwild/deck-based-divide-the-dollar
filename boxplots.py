import numpy as np
import glob
import matplotlib.pyplot as plt
import matplotlib.lines as mlines

fig, ax = plt.subplots(dpi=300)

color_list = [plt.cm.inferno(i)for i in np.linspace(0, 1, 5)]
purple = color_list[1]
orange = color_list[3]

bp_filename = 'bp-mut'
default = 'default 5 epi 15 pop 11 tsize 5 mut 8 states'

#folder = '15 pop 250 gens'
#folders = ('3 mutations','15 pop 250 gens','7 mutations','1 numepisodes','5 numepisodes','6 bda_states','15 pop 7 tsize','11 tsize','15 tsize','25 pop 250 gens','3 mut 7 tsize 5 epi','3 mut 11 tsize 5 epi','3 mut 11 tsize 1 epi')
folders = ('mut 3',default,'mut 7','mut 9')
#folders = ('mut 3',default,'mut 7','mut 9','mut 11','mut 13')
#folders = ('epi 1',default,'epi 10')
#folders = ('tsize 4','tsize 7',default,'tsize 15')
#folders = (default,'pop 25','pop 50')
#folders = ('3 mut 7 tsize 5 epi','3 mut 11 tsize 5 epi','3 mut 11 tsize 1 epi')
#folders = ('states 6',default,'states 12')

all_bp_data = []
for folder in folders:
#for gen in gens:
	path_name = '%s/win_percen-*.txt' % folder
	best_bp_data = []
	avg_bp_data = []
	for i, filename in enumerate(glob.iglob(path_name)):
		data = np.loadtxt(filename)
		best_bp_data.append(data[-1, -1])
		avg_bp_data.append(data[-1, 0])
		#bp_data.append([])
	all_bp_data.append(best_bp_data)
	all_bp_data.append(avg_bp_data)
	all_bp_data.append([])

bp = plt.boxplot(all_bp_data, notch=0, sym='+')
plt.setp(bp['whiskers'], color=purple)
plt.setp(bp['caps'], color=purple)

for i in xrange(0,12,3):
	plt.setp(bp['boxes'][i], color=purple)
	plt.setp(bp['boxes'][i+1], color=orange)
	plt.setp(bp['medians'][i], color=purple)
	plt.setp(bp['medians'][i+1], color=orange)
	bp['fliers'][i].set_markeredgecolor(purple)
	bp['fliers'][i+1].set_markeredgecolor(orange)
for i in xrange(0,19,6):
	plt.setp(bp['whiskers'][i+2], color=orange)
	plt.setp(bp['whiskers'][i+3], color=orange)
	plt.setp(bp['caps'][i+2], color=orange)
	plt.setp(bp['caps'][i+3], color=orange)
	

plt.ylim(0,1.0)
#plt.xlim(0,9)
plt.xlim(0,12)
#plt.xticks([1.5, 4.5, 7.5], ['3', '5', '7'])
plt.xticks([1.5, 4.5, 7.5, 10.5], ['3', '5', '7', '9'])
plt.xlabel('maximum number of mutations')
plt.ylabel('win percentage')

purple_line = mlines.Line2D([], [], color=purple, label='best')
orange_line = mlines.Line2D([], [], color=orange, label='average')
plt.legend(loc='lower right', handles=[purple_line,orange_line])

fig.savefig(bp_filename + '.eps', bbox_inches='tight', dpi=300)
#plt.show()