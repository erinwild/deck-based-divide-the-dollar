import numpy as np
import glob
import matplotlib.pyplot as plt
from scipy import stats


path_name = 'Evolving both/MC/policy_pi-*.txt'
for i, filename in enumerate(glob.iglob(path_name)):
	if i == 0:
		data = np.loadtxt(filename, dtype=np.int)
	else:
		data = np.column_stack((data, np.loadtxt(filename, dtype=np.int)))

bincounts = np.zeros((40,3), dtype=int)
for row, col in enumerate(data):
	col_counts = stats.itemfreq(col)
	for r in col_counts:
		bincounts[row,r[0]] = r[1]

action_analysis = np.loadtxt('action_analysis.txt')
card_counts = np.zeros((40,3))
for row in xrange(40):
	for col in xrange(3):
		if action_analysis[row,col] == 0.25:
			#if col == 2 and (row == 12 or row == 15): # adjust for hidden 0.5 card
			#	card_counts[row,0] += bincounts[row,col]/2.0
			#	card_counts[row,1] += bincounts[row,col]/2.0
			#else:
			card_counts[row,0] += bincounts[row,col]
		elif action_analysis[row,col] == 0.5:
			card_counts[row,1] += bincounts[row,col]
		elif action_analysis[row,col] == 0.75:
			#if col == 0 and (row == 22 or row == 25): # adjust for hidden 0.5 card
			#	card_counts[row,2] += bincounts[row,col]/2.0
			#	card_counts[row,1] += bincounts[row,col]/2.0
			#else:
			card_counts[row,2] += bincounts[row,col]
		
#print card_counts



fig, ax = plt.subplots(figsize=(6,6), dpi=300)
heatmap = ax.pcolor(card_counts[0:10], cmap='inferno_r')
#heatmap = ax.pcolor(bincounts[30:40], cmap='inferno_r')
heatmap.set_clim(vmin=0, vmax=100)
cbar = plt.colorbar(heatmap)
cbar.set_label('frequency of choice')
ax.set_xticks([0.5,1.5,2.5])
ax.set_xticklabels(['0.25','0.5','0.75'])
#ax.set_xticklabels(['small_spoil','median','large_max'])
ax.set_yticks([0.5,1.5,2.5,3.5,4.5,5.5,6.5,7.5,8.5,9.5])
ax.set_yticklabels(['[0.25,0.25,0.25]','[0.25,0.25,0.5]','[0.25,0.25,0.75]','[0.25,0.5,0.5]','[0.25,0.5,0.75]','[0.25,0.75,0.75]','[0.5,0.5,0.5]','[0.5,0.5,0.75]','[0.5,0.75,0.75]','[0.75,0.75,0.75]'])
ax.set_ylabel('hand state')
fig.subplots_adjust(wspace=0.2,left=0.22,bottom=0.1,right=0.90,top=0.9)

fig.savefig('action_analysis-mc_025showing.eps', bbox_inches='tight', dpi=300)
#plt.show()