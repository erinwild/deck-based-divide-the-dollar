from __future__ import division
import glob
import numpy as np
import scipy.stats as st
import time
import copy
import bda

start = time.clock()
#np.random.seed(1234)

## Parameters for divide-the-dollar game ##
cards = [0.25, 0.50, 0.75] # specifies the unique cards in the deck: indexed as [0,1,2]
num_of_unique_cards = [16, 28, 16] # specifies the total number of each of the unique cards
num_cards = len(cards) # number of unique cards
deck_size = sum(num_of_unique_cards) # total number of cards in the deck
num_players = 2 # number of players
hand_size = 5 # number of cards in a player's hand--must be odd
num_actions = bda.NUM_ACTIONS # 0->small_spoil, 1->median, 2->large_max

num_rounds = 1+(deck_size-(num_players*hand_size))//num_players # number of hands to play (until deck runs out)
num_episodes = 2000 # number of games to play between two players (to ensure fair deck shuffling over time)

true_state_index = np.loadtxt('true_state_index.txt')

def load_mc_players():
	mc_pop = []
	for filename in glob.iglob('Players/mc-pol-*.txt'):
		print filename
		mc_pop.append(np.loadtxt(filename))
	return mc_pop

def load_bda_players():
	bda_pop = []
	for filename in glob.iglob('Players/bda*.txt'):
		print filename
		bda_state_data = np.loadtxt(filename)
		bda_player = bda.BDA(0)
		bda_player.read_bda(bda_state_data)
		bda_pop.append(bda_player)
	return bda_pop

def load_deck():
	d = []
	for card, num in enumerate(num_of_unique_cards):
		d += [card]*num
	return np.array(d)

def play_action(card_showing, player, player_card_value, player_action):
	if card_showing == num_cards: # player's going first
		if player_action == 0:
			card_showing = player[0]
			player_card_value = cards[player[0]] # play smallest card
			player = np.delete(player, 0) # remove card from player's hand
		elif player_action == 2:
			card_showing = player[-1]
			player_card_value = cards[player[-1]] # play largest card
			player = np.delete(player, -1) # remove card from player's hand
		else:
			card_showing = player[hand_size//2]
			player_card_value = cards[player[hand_size//2]] # play median card
			player = np.delete(player, hand_size//2) # remove card from player's hand
	else: # opponent went first, player's turn
		if player_action == 0: # spoil with smallest card
			for c, pcard in enumerate(player):
				if cards[pcard] + cards[card_showing] > 1.0: # can spoil, play this card
					player_card_value = cards[player[c]]
					player = np.delete(player, c) # remove card from player's hand
					break
				elif c == len(player)-1: # can't spoil, play largest card
					player_card_value = cards[player[-1]]
					player = np.delete(player, -1) # remove card from player's hand
		elif player_action == 2: # maximize score with largest card
			for c, pcard in enumerate(np.flipud(player)):
				if cards[pcard] + cards[card_showing] <= 1.0: # can maximize, play this card
					player_card_value = cards[player[len(player)-1-c]]
					player = np.delete(player, len(player)-1-c) # remove card from player's hand
					break
				elif len(player)-c == 0: # can't maximize, play smallest card
					player_card_value = cards[player[0]]
					player = np.delete(player, 0) # remove card from player's hand
		else:
			player_card_value = cards[player[hand_size//2]] # play median card
			player = np.delete(player, hand_size//2) # remove card from player's hand
	return card_showing, player, player_card_value


mc_pop = load_mc_players()
bda_pop = load_bda_players()
mc_pop_size = len(mc_pop)
bda_pop_size = len(bda_pop)
pop_size = mc_pop_size+bda_pop_size

wins = np.zeros((pop_size,pop_size), dtype=int)
losses = np.zeros((pop_size,pop_size), dtype=int)
score_diff  = np.zeros((pop_size,pop_size))

for ep in xrange(1,num_episodes+1):
	
	for bda1_index in xrange(bda_pop_size):
		# BDA vs BDA
		for bda2_index in xrange(bda1_index+1,bda_pop_size):
			deck = load_deck()
			np.random.shuffle(deck)
			bda_pop[bda1_index].reset()
			bda_pop[bda2_index].reset()
			p1_total_score = 0
			p2_total_score = 0
			num_deals = 0
			p1_cards = np.sort(deck[:hand_size])
			deck = deck[hand_size:]
			p2_cards = np.sort(deck[:hand_size])
			deck = deck[hand_size:]
			for round_index in xrange(num_rounds):
				p1_card_value = 0
				p2_card_value = 0
				
				# Determine the value of the card showing (0 if playing first; opponent's pick if playing second)
				card_showing = num_cards # an index of 'num_cards' corresponds to no card showing (i.e. zero)
				if round_index % 2 == 0:
					# BDA 1 goes first
					p1_game_state = [0, p1_cards[0], p1_cards[hand_size//2], p1_cards[-1], num_deals/(round_index+1), 0]
					p1_action = bda_pop[bda1_index].run(p1_game_state)
					card_showing, p1_cards, p1_card_value = play_action(card_showing, p1_cards, p1_card_value, p1_action)
					
					# BDA 2 goes second
					p2_game_state = [cards[card_showing], p2_cards[0], p2_cards[hand_size//2], p2_cards[-1], num_deals/(round_index+1), 1]
					p2_action = bda_pop[bda2_index].run(p2_game_state)
					card_showing, p2_cards, p2_card_value = play_action(card_showing, p2_cards, p2_card_value, p2_action)
				else:
					# BDA 2 goes first
					p2_game_state = [0, p2_cards[0], p2_cards[hand_size//2], p2_cards[-1], num_deals/(round_index+1), 0]
					p2_action = bda_pop[bda2_index].run(p2_game_state)
					card_showing, p2_cards, p2_card_value = play_action(card_showing, p2_cards, p2_card_value, p2_action)
					
					# BDA 1 goes second
					p1_game_state = [cards[card_showing], p1_cards[0], p1_cards[hand_size//2], p1_cards[-1], num_deals/(round_index+1), 1]
					p1_action = bda_pop[bda1_index].run(p1_game_state)
					card_showing, p1_cards, p1_card_value = play_action(card_showing, p1_cards, p1_card_value, p1_action)
				
				# Determine score for playing this hand
				if p1_card_value + p2_card_value <= 1:
					p1_total_score += p1_card_value
					p2_total_score += p2_card_value
					num_deals += 1
				
				# If deck isn't empty, pick up new cards
				if len(deck) != 0:
					p1_cards = np.sort(np.append(p1_cards, deck[:1]))
					deck = deck[1:]
				if len(deck) != 0:
					p2_cards = np.sort(np.append(p2_cards, deck[:1]))
					deck = deck[1:]
			
			# Determine final winner of the game and give out reward (+score keeping)
			if p1_total_score > p2_total_score:
				wins[mc_pop_size+bda1_index,mc_pop_size+bda2_index] += 1
				losses[mc_pop_size+bda2_index,mc_pop_size+bda1_index] += 1
			elif p1_total_score < p2_total_score:
				losses[mc_pop_size+bda1_index,mc_pop_size+bda2_index] += 1
				wins[mc_pop_size+bda2_index,mc_pop_size+bda1_index] += 1
			score_diff[mc_pop_size+bda1_index,mc_pop_size+bda2_index] = p1_total_score - p2_total_score
			score_diff[mc_pop_size+bda2_index,mc_pop_size+bda1_index] = p2_total_score - p1_total_score
		
		# BDA vs MC
		for mc2_index in xrange(mc_pop_size):
			deck = load_deck()
			np.random.shuffle(deck)
			bda_pop[bda1_index].reset()
			p2_policy = mc_pop[mc2_index]
			p1_total_score = 0
			p2_total_score = 0
			num_deals = 0
			p1_cards = np.sort(deck[:hand_size])
			deck = deck[hand_size:]
			p2_cards = np.sort(deck[:hand_size])
			deck = deck[hand_size:]
			for round_index in xrange(num_rounds):
				p1_card_value = 0
				p2_card_value = 0
				
				# Determine the value of the card showing (0 if playing first; opponent's pick if playing second)
				card_showing = num_cards # an index of 'num_cards' corresponds to no card showing (i.e. zero)
				if round_index % 2 == 0:
					# BDA 1 goes first
					p1_game_state = [0, p1_cards[0], p1_cards[hand_size//2], p1_cards[-1], num_deals/(round_index+1), 0]
					p1_action = bda_pop[bda1_index].run(p1_game_state)
					card_showing, p1_cards, p1_card_value = play_action(card_showing, p1_cards, p1_card_value, p1_action)
					
					# MC 2 goes second
					p2_game_state = [card_showing, p2_cards[0], p2_cards[hand_size//2], p2_cards[-1]]
					p2_policy_index = true_state_index[int(np.ravel_multi_index(p2_game_state, dims=(num_cards+1, num_cards, num_cards, num_cards)))]
					p2_action = p2_policy[int(p2_policy_index)]
					card_showing, p2_cards, p2_card_value = play_action(card_showing, p2_cards, p2_card_value, p2_action)
				else:
					# MC 2 goes first
					p2_game_state = [0, p2_cards[0], p2_cards[hand_size//2], p2_cards[-1]]
					p2_policy_index = true_state_index[int(np.ravel_multi_index(p2_game_state, dims=(num_cards+1, num_cards, num_cards, num_cards)))]
					p2_action = p2_policy[int(p2_policy_index)]
					card_showing, p2_cards, p2_card_value = play_action(card_showing, p2_cards, p2_card_value, p2_action)
					
					# BDA 1 goes second
					p1_game_state = [cards[card_showing], p1_cards[0], p1_cards[hand_size//2], p1_cards[-1], num_deals/(round_index+1), 1]
					p1_action = bda_pop[bda1_index].run(p1_game_state)
					card_showing, p1_cards, p1_card_value = play_action(card_showing, p1_cards, p1_card_value, p1_action)
				
				# Determine score for playing this hand
				if p1_card_value + p2_card_value <= 1:
					p1_total_score += p1_card_value
					p2_total_score += p2_card_value
					num_deals += 1
				
				# If deck isn't empty, pick up new cards
				if len(deck) != 0:
					p1_cards = np.sort(np.append(p1_cards, deck[:1]))
					deck = deck[1:]
				if len(deck) != 0:
					p2_cards = np.sort(np.append(p2_cards, deck[:1]))
					deck = deck[1:]
			
			# Determine final winner of the game and give out reward (+score keeping)
			if p1_total_score > p2_total_score:
				wins[bda1_index+mc_pop_size, mc2_index] += 1
				losses[mc2_index, bda1_index+mc_pop_size] += 1
			elif p1_total_score < p2_total_score:
				losses[bda1_index+mc_pop_size, mc2_index] += 1
				wins[mc2_index, bda1_index+mc_pop_size] += 1
			score_diff[bda1_index+mc_pop_size, mc2_index] = p1_total_score - p2_total_score
			score_diff[mc2_index, bda1_index+mc_pop_size] = p2_total_score - p1_total_score
	
	for mc1_index in xrange(mc_pop_size):
		# MC vs MC
		for mc2_index in xrange(mc1_index+1,mc_pop_size):
			deck = load_deck()
			np.random.shuffle(deck)
			p1_policy = mc_pop[mc1_index]
			p2_policy = mc_pop[mc2_index]
			p1_total_score = 0
			p2_total_score = 0
			num_deals = 0
			p1_cards = np.sort(deck[:hand_size])
			deck = deck[hand_size:]
			p2_cards = np.sort(deck[:hand_size])
			deck = deck[hand_size:]
			for round_index in xrange(num_rounds):
				p1_card_value = 0
				p2_card_value = 0
				
				# Determine the value of the card showing (0 if playing first; opponent's pick if playing second)
				card_showing = num_cards # an index of 'num_cards' corresponds to no card showing (i.e. zero)
				if round_index % 2 == 0:
					# MC 1 goes first
					p1_game_state = [0, p1_cards[0], p1_cards[hand_size//2], p1_cards[-1]]
					p1_policy_index = true_state_index[int(np.ravel_multi_index(p1_game_state, dims=(num_cards+1, num_cards, num_cards, num_cards)))]
					p1_action = p1_policy[int(p1_policy_index)]
					card_showing, p1_cards, p1_card_value = play_action(card_showing, p1_cards, p1_card_value, p1_action)
					
					# MC 2 goes second
					p2_game_state = [card_showing, p2_cards[0], p2_cards[hand_size//2], p2_cards[-1]]
					p2_policy_index = true_state_index[int(np.ravel_multi_index(p2_game_state, dims=(num_cards+1, num_cards, num_cards, num_cards)))]
					p2_action = p2_policy[int(p2_policy_index)]
					card_showing, p2_cards, p2_card_value = play_action(card_showing, p2_cards, p2_card_value, p2_action)
				else:
					# MC 2 goes first
					p2_game_state = [0, p2_cards[0], p2_cards[hand_size//2], p2_cards[-1]]
					p2_policy_index = true_state_index[int(np.ravel_multi_index(p2_game_state, dims=(num_cards+1, num_cards, num_cards, num_cards)))]
					p2_action = p2_policy[int(p2_policy_index)]
					card_showing, p2_cards, p2_card_value = play_action(card_showing, p2_cards, p2_card_value, p2_action)
					
					# MC 1 goes second
					p1_game_state = [card_showing, p1_cards[0], p1_cards[hand_size//2], p1_cards[-1]]
					p1_policy_index = true_state_index[int(np.ravel_multi_index(p1_game_state, dims=(num_cards+1, num_cards, num_cards, num_cards)))]
					p1_action = p1_policy[int(p1_policy_index)]
					card_showing, p1_cards, p1_card_value = play_action(card_showing, p1_cards, p1_card_value, p1_action)
				
				# Determine score for playing this hand
				if p1_card_value + p2_card_value <= 1:
					p1_total_score += p1_card_value
					p2_total_score += p2_card_value
					num_deals += 1
				
				# If deck isn't empty, pick up new cards
				if len(deck) != 0:
					p1_cards = np.sort(np.append(p1_cards, deck[:1]))
					deck = deck[1:]
				if len(deck) != 0:
					p2_cards = np.sort(np.append(p2_cards, deck[:1]))
					deck = deck[1:]
			
			# Determine final winner of the game and give out reward (+score keeping)
			if p1_total_score > p2_total_score:
				wins[mc1_index, mc2_index] += 1
				losses[mc2_index, mc1_index] += 1
			elif p1_total_score < p2_total_score:
				losses[mc1_index, mc2_index] += 1
				wins[mc2_index, mc1_index] += 1
			score_diff[mc1_index, mc2_index] = p1_total_score - p2_total_score
			score_diff[mc2_index, mc1_index] = p2_total_score - p1_total_score
			
				#if ep in [10,50,100,200,500,1000,2000]:
					#np.savetxt('win_percen-%i.txt' % ep, wins/ep, fmt='%.6f')
					#np.savetxt('loss_percen-%i.txt' % ep, losses/ep, fmt='%.6f')
					#np.savetxt('loss_percen-%i.txt' % ep, losses/ep, fmt='%.6f')

np.savetxt('wins-%i.txt' % num_episodes, wins, fmt='%i')
np.savetxt('losses-%i.txt' % num_episodes, losses, fmt='%i')
np.savetxt('score_diff-%i.txt' % num_episodes, score_diff, fmt='%.2f')

end = time.clock()
print "%.2f minutes" % ((end-start)/60)