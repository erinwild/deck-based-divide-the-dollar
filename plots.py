import numpy as np
import glob
import matplotlib.pyplot as plt
import matplotlib.lines as mlines

#fig, ax = plt.subplots(ncols=1, dpi=300)

#time_filename = 'ot-tsize'

#color_list = plt.cm.Vega10(np.linspace(0, 1, 10))
color_list = [plt.cm.inferno(i) for i in np.linspace(0, 1, 5)]
colours = [color_list[2],color_list[1],color_list[3],color_list[0]]
#colours = [color_list[1],color_list[3],color_list[0]]

default = 'default 5 epi 15 pop 11 tsize 5 mut 8 states'

#folders = ('3 mutations','15 pop 250 gens','7 mutations','1 numepisodes','5 numepisodes','6 bda_states','15 pop 7 tsize','11 tsize','15 tsize','25 pop 250 gens','3 mut 7 tsize 5 epi','3 mut 11 tsize 5 epi','3 mut 11 tsize 1 epi')
#folders = ('mut 3',default,'mut 7')
#folders = ('mut 3',default,'mut 7','mut 9','mut 11','mut 13')
#folders = ('epi 1',default,'epi 10')
#folders = ('tsize 4','tsize 7',default,'tsize 15')
#folders = (default,'pop 25','pop 50')
#folders = ('3 mut 7 tsize 5 epi','3 mut 11 tsize 5 epi','3 mut 11 tsize 1 epi')
#folders = ('states 6',default,'states 12')

for line, folder in enumerate(folders):
	path_name = folder + '/win_percen-*.txt'
	count = 0
	for i, filename in enumerate(glob.iglob(path_name)):
		data = np.loadtxt(filename)
		avg_fit = data[:, 0:1]
		max_fit = data[:, 3:4]
		if i == 0:
			averages = np.array(avg_fit)
			best = np.array(max_fit)
		else:
			averages += avg_fit[:]
			best += max_fit[:]
		count = i + 1.0
		#plt.plot(max_fit[:, :], color=grey)

	averages = np.divide(averages, count)
	best = np.divide(best, count)
	plt.plot(averages[:, 0], color=colours[line], linestyle='--', linewidth=2)
	plt.plot(best[:, 0], color=colours[line], linestyle='-', linewidth=2)

plt.xlim(0,249)
plt.xticks([0,50,100,150,200,249], [0,50,100,150,200,250])
plt.ylim(0.4,0.92)
plt.xlabel('generation')
plt.ylabel('win percentage')

first_line = mlines.Line2D([], [], color=colours[0], label='t size = 4')
second_line = mlines.Line2D([], [], color=colours[1], label='t size = 7')
third_line = mlines.Line2D([], [], color=colours[2], label='t size = 11')
fourth_line = mlines.Line2D([], [], color=colours[3], label='t size = 15')
legend1 = plt.legend(loc='center right', handles=[first_line,second_line,third_line,fourth_line])

solid_line = mlines.Line2D([], [], ls='-', color=color_list[0], label='avg. best')
dashed_line = mlines.Line2D([], [], ls='--', color=color_list[0], label='avg. average')
plt.legend(loc='lower right', handles=[solid_line,dashed_line])

plt.gca().add_artist(legend1)

#fig.savefig(time_filename + '.eps', bbox_inches='tight', dpi=300)
plt.show()