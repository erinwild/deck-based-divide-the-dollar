import bda
import numpy as np
import glob
import matplotlib.pyplot as plt

cards = [0.25, 0.50, 0.75] # specifies the unique cards in the deck: indexed as [0,1,2]
played = [0.25, 0.50, 0.75, 0]

num_cards = len(cards)

true_state_index =  [0,1,2,-1,3,4,-1,-1,5,-1,-1,-1,-1,6,7,-1,-1,8,-1,-1,-1,-1,-1,-1,-1,-1,9,10,11,12,-1,13,14,-1,-1,15,-1,-1,-1,-1,16,17,-1,-1,18,-1,-1,-1,-1,-1,-1,-1,-1,19,20,21,22,-1,23,24,-1,-1,25,-1,-1,-1,-1,26,27,-1,-1,28,-1,-1,-1,-1,-1,-1,-1,-1,29,30,31,32,-1,33,34,-1,-1,35,-1,-1,-1,-1,36,37,-1,-1,38,-1,-1,-1,-1,-1,-1,-1,-1,39]

bincounts = np.zeros((40,3), dtype=int)

bda_guy = bda.BDA(8)
path_name = 'Evolving both/Best BDA/*pop-*.txt'
for i, filename in enumerate(glob.iglob(path_name)):
	bda_guy.read_bda(np.loadtxt(filename))
	game_state_index = 0
	for card_showing in xrange(num_cards+1):
		for smallest in xrange(num_cards):
			for median in xrange(smallest, num_cards):
				for largest in xrange(median, num_cards):
					if card_showing == num_cards:
						first_player = 0 # true
					else:
						first_player = 1 # false
					frac_deals = 0.5
					game_state = [played[card_showing], cards[smallest], cards[median], cards[largest], frac_deals, first_player]
					#game_state_index = int(true_state_index[int(np.ravel_multi_index([card_showing,smallest,median,largest], dims=(num_cards+1,num_cards,num_cards,num_cards)))])
					action_taken = bda_guy.run(game_state)
					bincounts[game_state_index,action_taken] += 1
					game_state_index += 1

print bincounts

action_analysis = np.loadtxt('action_analysis.txt')
card_counts = np.zeros((40,3))
for row in xrange(40):
	for col in xrange(3):
		if action_analysis[row,col] == 0.25:
			#if col == 2 and (row == 12 or row == 15): # adjust for hidden 0.5 card
			#	card_counts[row,0] += bincounts[row,col]/2.0
			#	card_counts[row,1] += bincounts[row,col]/2.0
			#else:
			card_counts[row,0] += bincounts[row,col]
		elif action_analysis[row,col] == 0.5:
			card_counts[row,1] += bincounts[row,col]
		elif action_analysis[row,col] == 0.75:
			#if col == 0 and (row == 22 or row == 25): # adjust for hidden 0.5 card
			#	card_counts[row,2] += bincounts[row,col]/2.0
			#	card_counts[row,1] += bincounts[row,col]/2.0
			#else:
			card_counts[row,2] += bincounts[row,col]

print card_counts
			
fig, ax = plt.subplots(figsize=(6,6), dpi=300)
heatmap = ax.pcolor(card_counts[20:30], cmap='inferno_r')
#heatmap = ax.pcolor(bincounts[30:40], cmap='inferno_r')
heatmap.set_clim(vmin=0, vmax=100)
cbar = plt.colorbar(heatmap)
cbar.set_label('frequency of choice')
ax.set_xticks([0.5,1.5,2.5])
ax.set_xticklabels(['0.25','0.5','0.75'])
#ax.set_xticklabels(['small_spoil','median','large_max'])
ax.set_yticks([0.5,1.5,2.5,3.5,4.5,5.5,6.5,7.5,8.5,9.5])
ax.set_yticklabels(['[0.25,0.25,0.25]','[0.25,0.25,0.5]','[0.25,0.25,0.75]*','[0.25,0.5,0.5]','[0.25,0.5,0.75]','[0.25,0.75,0.75]*','[0.5,0.5,0.5]','[0.5,0.5,0.75]','[0.5,0.75,0.75]','[0.75,0.75,0.75]'])
ax.set_ylabel('hand state')
fig.subplots_adjust(wspace=0.2,left=0.22,bottom=0.1,right=0.90,top=0.9)

fig.savefig('action_analysis-bda_075showing.eps', bbox_inches='tight', dpi=300)
#plt.show()

