from __future__ import division
import numpy as np
import scipy.stats as st
import math
import time
import copy
import bda # class for BDA object
import mc # class for MC object

start = time.clock()

## Parameters for divide-the-dollar game ##
cards = [0.25, 0.50, 0.75] # specifies the unique cards in the deck: indexed as [0,1,2]
num_of_unique_cards = [16, 28, 16] # specifies the total number of each of the unique cards
num_cards = len(cards) # number of unique cards
deck_size = sum(num_of_unique_cards) # total number of cards in the deck
num_players = 2 # number of players
hand_size = 5 # number of cards in a player's hand--must be odd
num_rounds = 1+(deck_size-(num_players*hand_size))//num_players # number of hands to play (until deck runs out)
num_episodes = 100 # number of games to play between two players (to ensure fair deck shuffling over time)

## Parameters for BDA specification ##
bda_states = 8
num_actions = bda.NUM_ACTIONS # 0->small_spoil, 1->median, 2->large_max

## Parameters for MC specification ##
num_states = int((num_cards+1)*(math.factorial(3+num_cards-1))/(math.factorial(3)*math.factorial(num_cards-1)))
true_state_index = np.loadtxt('true_state_index.txt')
# true_state_index =  [0,1,2,-1,3,4,-1,-1,5,-1,-1,-1,-1,6,7,-1,-1,8,-1,-1,-1,-1,-1,-1,-1,-1,9,10,11,12,-1,13,14,-1,-1,15,-1,-1,-1,-1,16,17,-1,-1,18,-1,-1,-1,-1,-1,-1,-1,-1,19,20,21,22,-1,23,24,-1,-1,25,-1,-1,-1,-1,26,27,-1,-1,28,-1,-1,-1,-1,-1,-1,-1,-1,29,30,31,32,-1,33,34,-1,-1,35,-1,-1,-1,-1,36,37,-1,-1,38,-1,-1,-1,-1,-1,-1,-1,-1,39]

## Parameters for evolution ##
pop_size = 25
t_size = 11
max_mutations = 9
num_gens = 250
num_runs = 100

def init_bda_pop():
	pop = []
	for i in xrange(pop_size):
		pop.append(bda.BDA(bda_states))
		pop[i].randomize()
	return pop

def init_mc():
	monte = mc.MC(num_states, num_actions)
	return monte

def load_deck():
	d = []
	for card, num in enumerate(num_of_unique_cards):
		d += [card]*num
	return np.array(d)

def play_action(card_showing, player, player_card_value, player_action):
	if card_showing == num_cards: # player's going first
		if player_action == 0:
			card_showing = player[0]
			player_card_value = cards[player[0]] # play smallest card
			player = np.delete(player, 0) # remove card from player's hand
		elif player_action == 2:
			card_showing = player[-1]
			player_card_value = cards[player[-1]] # play largest card
			player = np.delete(player, -1) # remove card from player's hand
		else:
			card_showing = player[hand_size//2]
			player_card_value = cards[player[hand_size//2]] # play median card
			player = np.delete(player, hand_size//2) # remove card from player's hand
	else: # opponent went first, player's turn
		if player_action == 0: # spoil with smallest card
			for c, pcard in enumerate(player):
				if cards[pcard] + cards[card_showing] > 1.0: # can spoil, play this card
					player_card_value = cards[player[c]]
					player = np.delete(player, c) # remove card from player's hand
					break
				elif c == len(player)-1: # can't spoil, play largest card
					player_card_value = cards[player[-1]]
					player = np.delete(player, -1) # remove card from player's hand
		elif player_action == 2: # maximize score with largest card
			for c, pcard in enumerate(np.flipud(player)):
				if cards[pcard] + cards[card_showing] <= 1.0: # can maximize, play this card
					player_card_value = cards[player[len(player)-1-c]]
					player = np.delete(player, len(player)-1-c) # remove card from player's hand
					break
				elif len(player)-c == 0: # can't maximize, play smallest card
					player_card_value = cards[player[0]]
					player = np.delete(player, 0) # remove card from player's hand
		else:
			player_card_value = cards[player[hand_size//2]] # play median card
			player = np.delete(player, hand_size//2) # remove card from player's hand
	return card_showing, player, player_card_value

def save_pop(run, pop, fit):
	pop_file = open('pop-%i.txt' % run, 'w')
	first = True
	for i in np.argsort(fit)[0:][::-1]:
		if first:
			pop_file.write('%s\n\n' % bda_pop[i].write_bda())
			first = False
		pop_file.write('%.6f -fitness\n%s\n\n' % (fit[i],bda_pop[i].print_bda()))
	pop_file.close()

def report_fit_stats(stats_file, run, fit):
	mean = np.mean(fit)
	ci = st.t.interval(0.95, len(fit)-1, loc=mean, scale=st.sem(fit))
	std = np.std(fit)
	best = np.amax(fit)
	stats_file.write('%.6f %.6f %.6f %.6f\n' % (mean, ci[1], std, best))


for run in xrange(num_runs):
	print 'run %i' % run
	win_percen_file = open('win_percen-%i.txt' % run, 'w')
	score_diff_file = open('score_diff-%i.txt' % run, 'w')
	mc_win_percen_file = open('mc_win_percen-%i.txt' % run, 'w')
	mc_score_diff_file = open('mc_score_diff-%i.txt' % run, 'w')
	bda_pop = init_bda_pop()
	monte = init_mc()
	for gen in xrange(num_gens):
		print 'gen %i' % gen
		
		## (fitness) score-keeping
		wins = np.array([0 for i in xrange(pop_size+1)])
		losses = np.array([0 for i in xrange(pop_size+1)])
		draws = np.array([0 for i in xrange(pop_size+1)])
		score_diff = np.array([0 for i in xrange(pop_size+1)])
		
		#mc_policy = copy.deepcopy(monte.policy_pi)
		
		## Round-robin Match-ups ##
		shuf_pop = np.arange(pop_size)
		np.random.shuffle(shuf_pop)
		for bda_index in shuf_pop: # Player 1 - evolving
			for ep in xrange(num_episodes):
				# Load and shuffle deck
				deck = load_deck()
				np.random.shuffle(deck)
				
				bda_pop[bda_index].reset()
				bda_total_score = 0
				mc_total_score = 0
				num_deals = 0 # number of times a round resulted in a positive score for both players
				
				# Deal initial hands
				bda_cards = np.sort(deck[:hand_size])
				deck = deck[hand_size:]
				mc_cards = np.sort(deck[:hand_size])
				deck = deck[hand_size:]
				
				monte.clear_states_seen()
				
				for round_index in xrange(num_rounds):
					bda_card_value = 0
					mc_card_value = 0
					
					# Determine the value of the card showing (0 if playing first; opponent's pick if playing second)
					card_showing = num_cards # an index of 'num_cards' corresponds to no card showing (i.e. zero)
					
					if round_index % 2 == 0:
						# Player 1 (BDA) goes first
						bda_game_state = [0, bda_cards[0], bda_cards[hand_size//2], bda_cards[-1], num_deals/(round_index+1), 0]
						bda_action = bda_pop[bda_index].run(bda_game_state)
						card_showing, bda_cards, bda_card_value = play_action(card_showing, bda_cards, bda_card_value, bda_action)
						
						# Player 2 (MC) goes second
						mc_game_state = [card_showing, mc_cards[0], mc_cards[hand_size//2], mc_cards[-1]]
						monte.record_state_seen(mc_game_state)
						mc_policy_index = true_state_index[int(np.ravel_multi_index(mc_game_state, dims=(num_cards+1, num_cards, num_cards, num_cards)))]
						if round_index <= 1:
							monte.policy_pi[int(mc_policy_index)] = np.random.randint(num_actions,size=1)[0] # for exploring starts take an initial random policy
						mc_action = monte.policy_pi[int(mc_policy_index)]
						card_showing, mc_cards, mc_card_value = play_action(card_showing, mc_cards, mc_card_value, mc_action)
					else:
						# Player 2 (MC) goes first
						mc_game_state = [card_showing, mc_cards[0], mc_cards[hand_size//2], mc_cards[-1]]
						monte.record_state_seen(mc_game_state)
						mc_policy_index = true_state_index[int(np.ravel_multi_index(mc_game_state, dims=(num_cards+1, num_cards, num_cards, num_cards)))]
						if round_index <= 1:
							monte.policy_pi[int(mc_policy_index)] = np.random.randint(num_actions,size=1)[0] # for exploring starts take an initial random policy
						mc_action = monte.policy_pi[int(mc_policy_index)]
						card_showing, mc_cards, mc_card_value = play_action(card_showing, mc_cards, mc_card_value, mc_action)
					
						# Player 1 (BDA) goes second
						bda_game_state = [cards[card_showing], bda_cards[0], bda_cards[hand_size//2], bda_cards[-1], num_deals/(round_index+1), 1]
						bda_action = bda_pop[bda_index].run(bda_game_state)
						card_showing, bda_cards, bda_card_value = play_action(card_showing, bda_cards, bda_card_value, bda_action)
					
					# Determine score for playing this hand
					if bda_card_value + mc_card_value <= 1:
						bda_total_score += bda_card_value
						mc_total_score += mc_card_value
						num_deals += 1
					
					# If deck isn't empty, pick up new cards
					if len(deck) != 0:
						bda_cards = np.sort(np.append(bda_cards, deck[:1]))
						deck = deck[1:]
					if len(deck) != 0:
						mc_cards = np.sort(np.append(mc_cards, deck[:1]))
						deck = deck[1:]
				
				# Determine final winner of the game and give out reward (+score keeping)
				rew = 0
				if bda_total_score > mc_total_score:
					wins[bda_index] += 1
					losses[-1] += 1
					rew = -1
				elif bda_total_score < mc_total_score:
					losses[bda_index] += 1
					wins[-1] += 1
					rew = 1
				else:
					draws[bda_index] += 1
					draws[-1] += 1
				score_diff[bda_index] += bda_total_score - mc_total_score
				score_diff[-1] += mc_total_score - bda_total_score
				rew = mc_total_score - bda_total_score # different reward system
				
				# Accumulate these values used in computing statistics on this action value function Q^pi
				for state_index in xrange(len(monte.state_seen)):
					sta_ind = int(true_state_index[int(np.ravel_multi_index(monte.state_seen[state_index], dims=(num_cards+1,num_cards,num_cards,num_cards)))])
					act_ind = int(true_state_index[int(monte.policy_pi[sta_ind])])
					monte.update(sta_ind, act_ind, rew)
		
		mc_score_diff_file.write("%.2f\n" % score_diff[-1])
		mc_fit = wins[-1]/(pop_size*num_episodes)
		mc_draws = draws[-1]/(pop_size*num_episodes)
		mc_win_percen_file.write('%.6f %.6f\n' % (mc_fit, mc_draws))
		
		fit = wins[0:pop_size]/num_episodes # use win percentage as fitness
		report_fit_stats(win_percen_file, run, fit) # save information about fitness for this generation
		report_fit_stats(score_diff_file, run, score_diff[0:pop_size])
		if gen == num_gens-1:
			#save_pop(run, bda_pop, fit)
			pop_file = open('pop-%i.txt' % run, 'w')
			for i in np.argsort(fit)[0:][::-1]:
				pop_file.write('%.6f -fitness (%.2f)\n%s\n\n' % (fit[i], score_diff[i], bda_pop[i].print_bda()))
			pop_file.close()
		else: ## Evolution time ##
			# Choose and sort the mating tournament participants
			dx = np.random.permutation(len(fit)) # sorting index
			dx[:t_size] = dx[:t_size][fit[dx][:t_size].argsort()]
			
			# Crossover (replace worst two with crossover result of best two)
			bda_pop[dx[0]] = copy.deepcopy(bda_pop[dx[t_size-1]])
			bda_pop[dx[1]] = copy.deepcopy(bda_pop[dx[t_size-2]])
			bda_pop[dx[0]].two_point_crossover(bda_pop[dx[1]])
			# Mutation
			for m in xrange(max_mutations):
				bda_pop[dx[0]].mutate()
				bda_pop[dx[1]].mutate()
		
	win_percen_file.close()
	score_diff_file.close()
	mc_win_percen_file.close()
	mc_score_diff_file.close()
	
	monte.print_mc(run)

end = time.clock()
print "%.2f minutes" % ((end-start)/60)