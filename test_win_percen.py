import numpy as np
import matplotlib.pyplot as plt

player_order = ['mc-pol-allexp', 'mc-pol-diffrew', 'mc-pol-evolveboth-1', 'mc-pol-evolveboth-2', 'mc-pol-evolveboth-3', 'mc-pol-evolveboth-4', 'mc-pol-evolveboth-5', 'mc-pol-lessexp', 'mc-pol-selfplay', 'mc-pol-sixrounds', 'mc-pol-tworounds', 'bda-default-1', 'bda-default-2', 'bda-default-3', 'bda-default-4', 'bda-default-5', 'bda-epi1-1', 'bda-epi1-2', 'bda-epi1-3', 'bda-epi1-4', 'bda-epi1-5', 'bda-epi10-1', 'bda-epi10-2', 'bda-epi10-3', 'bda-epi10-4', 'bda-epi10-5', 'bda-evolve-both-1', 'bda-evolve-both-2', 'bda-evolve-both-3', 'bda-evolve-both-4', 'bda-evolve-both-5', 'bda-mut11-1', 'bda-mut11-2', 'bda-mut11-3', 'bda-mut11-4', 'bda-mut11-5', 'bda-mut13-1', 'bda-mut13-2', 'bda-mut13-3', 'bda-mut13-4', 'bda-mut13-5', 'bda-mut3-1', 'bda-mut3-2', 'bda-mut3-3', 'bda-mut3-4', 'bda-mut3-5', 'bda-mut7-1', 'bda-mut7-2', 'bda-mut7-3', 'bda-mut7-4', 'bda-mut7-5', 'bda-mut9-1', 'bda-mut9-2', 'bda-mut9-3', 'bda-mut9-4', 'bda-mut9-5', 'bda-pop25-1', 'bda-pop25-2', 'bda-pop25-3', 'bda-pop25-4', 'bda-pop25-5', 'bda-pop50-1', 'bda-pop50-2', 'bda-pop50-3', 'bda-pop50-4', 'bda-pop50-5', 'bda-rand-1', 'bda-rand-2', 'bda-rand-3', 'bda-rand-4', 'bda-rand-5', 'bda-states12-1', 'bda-states12-2', 'bda-states12-3', 'bda-states12-4', 'bda-states12-5', 'bda-states6-1', 'bda-states6-2', 'bda-states6-3', 'bda-states6-4', 'bda-states6-5', 'bda-tsize15-1', 'bda-tsize15-2', 'bda-tsize15-3', 'bda-tsize15-4', 'bda-tsize15-5', 'bda-tsize4-1', 'bda-tsize4-2', 'bda-tsize4-3', 'bda-tsize4-4', 'bda-tsize4-5', 'bda-tsize7-1', 'bda-tsize7-2', 'bda-tsize7-3', 'bda-tsize7-4', 'bda-tsize7-5']

win_percen = np.loadtxt('win_percen-1000.txt')
#win_percen = np.delete(win_percen,[i for i in xrange(0,11)],axis=1)
#win_percen = np.delete(win_percen,[i for i in xrange(0,11)],axis=0)
#player_order = np.delete(player_order,[i for i in xrange(0,11)])
#print player_order
#loss_percen = np.loadtxt('loss_percen-1000.txt')
#windraw_percen = 1-loss_percen
#draw_percen = 1-win_percen-loss_percen
#avg_windraw_percen = np.sum(windraw_percen, axis=1)/(len(windraw_percen)-1)

avg_win_percen = np.sum(win_percen, axis=1)/(len(win_percen)-1)


sort_index = np.argsort(avg_win_percen)
#for i in sort_index:
	#print player_order[i], ':', avg_win_percen[i]

#fig, ax = plt.subplots()#figsize=(6,6), dpi=300)
#heatmap = ax.pcolor(win_percen, cmap='viridis_r')
#heatmap = ax.pcolor(np.column_stack((np.ones(len(avg_win_percen)),avg_win_percen)), cmap='viridis_r')
#heatmap.set_clim(vmin=0, vmax=1)
#cbar = plt.colorbar(heatmap)
#cbar.set_label('win percentage')


#ax.set_yticks([0,11,16,26,31,46,56,61,71,86])
#ax.set_yticklabels(['','MC','default','epi','evolve_both','mut','pop','rand','states','tsize'])

#ax.set_xticks([0,11,16,26,31,46,56,61,71,86])
#ax.set_xticklabels(['','MC','default','epi','evolve_both','mut','pop','rand','states','tsize'])

#ax.set_xlabel('vs. opponents')
#ax.set_ylabel('player')

#wins_vs_rand = []
#losses_vs_rand = []
#for i in xrange(1,2):
#	if i not in [66,67,68,69,70]:
#		wins_vs_rand.append(win_percen[i][66:71])
#		losses_vs_rand.append(win_percen.T[i][66:71])
#avg_wins_vs_rand = np.mean(np.array(wins_vs_rand), axis=0)
#avg_losses_vs_rand = np.mean(np.array(losses_vs_rand), axis=0)
#print avg_wins_vs_rand
#print avg_losses_vs_rand

## Boxplots
fig, ax = plt.subplots()#dpi=300)
color_list = [plt.cm.inferno(i)for i in np.linspace(0, 1, 5)]
black = color_list[0]
pink = color_list[2]
orange = color_list[3]
mc = avg_win_percen[:11]
#diffrew = mc[1]
#mc = np.delete(mc,1)
#print mc
bda = avg_win_percen[11:]
rand = bda[55:60]
bda = np.delete(bda,[i for i in xrange(55,60)],axis=0)
#print bda
#bp = plt.boxplot([mc,bda], notch=0, sym='+')
plt.plot([1], [mc[1:]], marker='+', markersize=7, color=pink)
plt.plot([2], [bda[1:]], marker='+', markersize=7, color=black)
plt.plot([3], [rand[1:]], marker='+', markersize=7, color=orange)
#plt.legend(loc='upper right')
ax.set_xticks([1,2,3])
ax.set_xticklabels(['MC','BDA','random'])
plt.xlim(0,4)
plt.ylim(0,1)
ax.set_ylabel('win percentage')
ax.set_xlabel('player')
##

fig.savefig('win_percen-1000.eps', bbox_inches='tight', dpi=300)

#plt.show()