import numpy as np
import glob
import matplotlib.pyplot as plt
import matplotlib.lines as mlines

fig, ax = plt.subplots(ncols=1, dpi=300)

time_filename = 'ot-evolving_both-diff'

color_list = [plt.cm.inferno(i)for i in np.linspace(0, 1, 5)]
colours = [color_list[2],color_list[1],color_list[3],color_list[0]]


path_name = 'Evolving both/win_percen-*.txt'
count = 0
for i, filename in enumerate(glob.iglob(path_name)):
	data = np.loadtxt(filename)
	avg_fit = data[:, 0:1]
	max_fit = data[:, 3:4]
	if i == 0:
		averages = np.array(avg_fit)
		best = np.array(max_fit)
	else:
		averages += avg_fit[:]
		best += max_fit[:]
	count = i + 1.0
	#plt.plot(max_fit[:, :], color=grey)

averages = np.divide(averages, count)
best = np.divide(best, count)
plt.plot(averages[:, 0], color=colours[3], linestyle='--', linewidth=2)
plt.plot(best[:, 0], color=colours[3], linestyle='-', linewidth=2)

path_name = 'Evolving both/Diffrew MC/mc_win_percen-*.txt'
count = 0
for i, filename in enumerate(glob.iglob(path_name)):
	data = np.loadtxt(filename)
	fit = data[:, 0:1]
	if i == 0:
		averages = np.array(fit)
	else:
		averages += fit[:]
	count = i + 1.0
	#plt.plot(max_fit[:, :], color=grey)

averages = np.divide(averages, count)
plt.plot(averages[:, 0], color=colours[0], linestyle='-', linewidth=2)

plt.xlim(0,249)
plt.xticks([0,50,100,150,200,249], [0,50,100,150,200,250])
plt.ylim(0,1.0)
plt.xlabel('generation')
plt.ylabel('win percentage')

#first_line = mlines.Line2D([], [], color=colours[0], label='t size = 4')
#second_line = mlines.Line2D([], [], color=colours[1], label='t size = 7')
third_line = mlines.Line2D([], [], color=colours[3], label='BDA agents')
fourth_line = mlines.Line2D([], [], color=colours[0], label='MC agents')
legend1 = plt.legend(loc='upper right', handles=[third_line,fourth_line])

solid_line = mlines.Line2D([], [], ls='-', color=color_list[0], label='avg. best')
dashed_line = mlines.Line2D([], [], ls='--', color=color_list[0], label='avg. average')
plt.legend(loc='lower right', handles=[solid_line,dashed_line])

plt.gca().add_artist(legend1)

fig.savefig(time_filename + '.eps', bbox_inches='tight', dpi=300)
#plt.show()