import numpy as np
import glob
import matplotlib.pyplot as plt
import matplotlib.lines as mlines

fig, ax = plt.subplots(ncols=1, dpi=300)

color_list = [plt.cm.inferno(i)for i in np.linspace(0, 1, 5)]
purple = color_list[1]
orange = color_list[3]

bp_filename = 'bp-evolving_both-diff'

all_bp_data = []

path_name = 'Evolving both/win_percen-*.txt'
best_bp_data = []
avg_bp_data = []
for i, filename in enumerate(glob.iglob(path_name)):
	data = np.loadtxt(filename)
	best_bp_data.append(data[-1, -1])
	avg_bp_data.append(data[-1, 0])
all_bp_data.append(best_bp_data)
all_bp_data.append(avg_bp_data)
all_bp_data.append([])

path_name = 'Evolving both/Diffrew MC/mc_win_percen-*.txt'
mc_bp_data = []
for i, filename in enumerate(glob.iglob(path_name)):
	data = np.loadtxt(filename)
	mc_bp_data.append(data[-1, 0])
all_bp_data.append(mc_bp_data)
all_bp_data.append([])

bp = plt.boxplot(all_bp_data, notch=0, sym='+')
plt.setp(bp['whiskers'], color=purple)
plt.setp(bp['caps'], color=purple)

for i in xrange(0,5,3):
	plt.setp(bp['boxes'][i], color=purple)
	plt.setp(bp['boxes'][i+1], color=orange)
	plt.setp(bp['medians'][i], color=purple)
	plt.setp(bp['medians'][i+1], color=orange)
	bp['fliers'][i].set_markeredgecolor(purple)
	bp['fliers'][i+1].set_markeredgecolor(orange)
for i in xrange(0,10,6):
	plt.setp(bp['whiskers'][i+2], color=orange)
	plt.setp(bp['whiskers'][i+3], color=orange)
	plt.setp(bp['caps'][i+2], color=orange)
	plt.setp(bp['caps'][i+3], color=orange)
	

plt.ylim(0,1.0)
#plt.xlim(0,9)
plt.xlim(0,5)
#plt.xticks([1.5, 4.5, 7.5], ['3', '5', '7'])
plt.xticks([1.5, 4], ['BDA', 'MC'])
plt.xlabel('agent representation')
plt.ylabel('win percentage')

purple_line = mlines.Line2D([], [], color=purple, label='best')
orange_line = mlines.Line2D([], [], color=orange, label='average')
plt.legend(loc='lower left', handles=[purple_line,orange_line])

fig.savefig(bp_filename + '.eps', bbox_inches='tight', dpi=300)
#plt.show()